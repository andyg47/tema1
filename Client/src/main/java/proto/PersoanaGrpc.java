package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition. 
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: persoana.proto")
public final class PersoanaGrpc {

  private PersoanaGrpc() {}

  public static final String SERVICE_NAME = "Persoana";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.PersoanaOuterClass.RequestNameAndCNP,
      proto.PersoanaOuterClass.NumeSiCNPReply> getNumeSiCNPMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "NumeSiCNP",
      requestType = proto.PersoanaOuterClass.RequestNameAndCNP.class,
      responseType = proto.PersoanaOuterClass.NumeSiCNPReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.PersoanaOuterClass.RequestNameAndCNP,
      proto.PersoanaOuterClass.NumeSiCNPReply> getNumeSiCNPMethod() {
    io.grpc.MethodDescriptor<proto.PersoanaOuterClass.RequestNameAndCNP, proto.PersoanaOuterClass.NumeSiCNPReply> getNumeSiCNPMethod;
    if ((getNumeSiCNPMethod = PersoanaGrpc.getNumeSiCNPMethod) == null) {
      synchronized (PersoanaGrpc.class) {
        if ((getNumeSiCNPMethod = PersoanaGrpc.getNumeSiCNPMethod) == null) {
          PersoanaGrpc.getNumeSiCNPMethod = getNumeSiCNPMethod = 
              io.grpc.MethodDescriptor.<proto.PersoanaOuterClass.RequestNameAndCNP, proto.PersoanaOuterClass.NumeSiCNPReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Persoana", "NumeSiCNP"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.PersoanaOuterClass.RequestNameAndCNP.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.PersoanaOuterClass.NumeSiCNPReply.getDefaultInstance()))
                  .setSchemaDescriptor(new PersoanaMethodDescriptorSupplier("NumeSiCNP"))
                  .build();
          }
        }
     }
     return getNumeSiCNPMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PersoanaStub newStub(io.grpc.Channel channel) {
    return new PersoanaStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PersoanaBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new PersoanaBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PersoanaFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new PersoanaFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static abstract class PersoanaImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public void numeSiCNP(proto.PersoanaOuterClass.RequestNameAndCNP request,
        io.grpc.stub.StreamObserver<proto.PersoanaOuterClass.NumeSiCNPReply> responseObserver) {
      asyncUnimplementedUnaryCall(getNumeSiCNPMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getNumeSiCNPMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.PersoanaOuterClass.RequestNameAndCNP,
                proto.PersoanaOuterClass.NumeSiCNPReply>(
                  this, METHODID_NUME_SI_CNP)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class PersoanaStub extends io.grpc.stub.AbstractStub<PersoanaStub> {
    private PersoanaStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoanaStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoanaStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoanaStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public void numeSiCNP(proto.PersoanaOuterClass.RequestNameAndCNP request,
        io.grpc.stub.StreamObserver<proto.PersoanaOuterClass.NumeSiCNPReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNumeSiCNPMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class PersoanaBlockingStub extends io.grpc.stub.AbstractStub<PersoanaBlockingStub> {
    private PersoanaBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoanaBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoanaBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoanaBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public proto.PersoanaOuterClass.NumeSiCNPReply numeSiCNP(proto.PersoanaOuterClass.RequestNameAndCNP request) {
      return blockingUnaryCall(
          getChannel(), getNumeSiCNPMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class PersoanaFutureStub extends io.grpc.stub.AbstractStub<PersoanaFutureStub> {
    private PersoanaFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoanaFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoanaFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoanaFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.PersoanaOuterClass.NumeSiCNPReply> numeSiCNP(
        proto.PersoanaOuterClass.RequestNameAndCNP request) {
      return futureUnaryCall(
          getChannel().newCall(getNumeSiCNPMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_NUME_SI_CNP = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PersoanaImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PersoanaImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_NUME_SI_CNP:
          serviceImpl.numeSiCNP((proto.PersoanaOuterClass.RequestNameAndCNP) request,
              (io.grpc.stub.StreamObserver<proto.PersoanaOuterClass.NumeSiCNPReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PersoanaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PersoanaBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.PersoanaOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Persoana");
    }
  }

  private static final class PersoanaFileDescriptorSupplier
      extends PersoanaBaseDescriptorSupplier {
    PersoanaFileDescriptorSupplier() {}
  }

  private static final class PersoanaMethodDescriptorSupplier
      extends PersoanaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PersoanaMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PersoanaGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PersoanaFileDescriptorSupplier())
              .addMethod(getNumeSiCNPMethod())
              .build();
        }
      }
    }
    return result;
  }
}
