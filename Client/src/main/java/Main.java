import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.PersoanaGrpc;
import proto.PersoanaOuterClass;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 4444).usePlaintext().build();

        PersoanaGrpc.PersoanaBlockingStub persoanaStub = PersoanaGrpc.newBlockingStub(channel);
        boolean running = true;
        while(running) {
            boolean ok = true;
            Scanner sc = new Scanner(System.in);
            System.out.println("Introduceti numele si CNP-ul.");
            String nume = sc.nextLine(); //5000510290270
            String cnp = sc.nextLine();  //01234567
            Pattern pattern = Pattern.compile("[^a-z ]", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(nume);
            boolean matchFound = matcher.find();
            if(cnp.length()==13 && !matchFound)
            {
                if ((cnp.charAt(0) == '1' || cnp.charAt(0) == '2' || cnp.charAt(0) == '5' || cnp.charAt(0) == '6' ) && (Integer.parseInt(cnp.substring(3,5)) < 13) && (Integer.parseInt(cnp.substring(5,7)) < 32)) {
                    for (int index = 0; index < cnp.length(); index++) {
                        if (cnp.charAt(index) < '0' || cnp.charAt(index) > '9')
                            ok = false;
                    }
                } else ok = false;
            }
            else ok =false;
            if (ok) {
                persoanaStub.numeSiCNP(PersoanaOuterClass.RequestNameAndCNP.newBuilder().setCnp(cnp).setName(nume).build());
                System.out.println("Date trimise cu succes.");
            }
            else System.out.println("Datele nu au fost trimise deoarece sunt invalide.");
        }


        /* *
         * We can now use gRPC function via our instance of GreeterBlockingStub bookStub.
         * Below we call the function sayHello(Helloworld.HelloRequest request) with name field value set to "gRPC".
         * This function return a value of type  Helloworld.HelloReply that is saved in our instance reply.
         * We can get via generated functions every field from our message, in this case we have just one field.
         * */
        //Persoana.HelloReply reply = bookStub.sayHello(Helloworld.HelloRequest.newBuilder().setName("gRPC").build());
        //System.out.println(reply.getMessage());

        channel.shutdown();
    }
}
