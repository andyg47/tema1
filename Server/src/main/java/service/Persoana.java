package service;

import io.grpc.stub.StreamObserver;
import proto.PersoanaGrpc;
import proto.PersoanaOuterClass;

public class Persoana extends PersoanaGrpc.PersoanaImplBase {
    @Override
    public void numeSiCNP(PersoanaOuterClass.RequestNameAndCNP request, StreamObserver<PersoanaOuterClass.NumeSiCNPReply> responseObserver) {
        System.out.println(request);
        if(request.getCnp().charAt(0)=='1') {
            System.out.println(request.getName());
            System.out.println("Barbat nascut in " + request.getCnp().charAt(5) + request.getCnp().charAt(6) + "/" + request.getCnp().charAt(3) + request.getCnp().charAt(4) + "/19" + request.getCnp().charAt(1) + request.getCnp().charAt(2));
        }
        else if(request.getCnp().charAt(0)=='5') {
            System.out.println(request.getName());
            System.out.println("Barbat nascut in " + request.getCnp().charAt(5) + request.getCnp().charAt(6) + "/" + request.getCnp().charAt(3) + request.getCnp().charAt(4) + "/20" + request.getCnp().charAt(1) + request.getCnp().charAt(2));
        }
        else if(request.getCnp().charAt(0)=='2') {
            System.out.println(request.getName());
            System.out.println("Femeie nascuta in " + request.getCnp().charAt(5) + request.getCnp().charAt(6) + "/" + request.getCnp().charAt(3) + request.getCnp().charAt(4) + "/19" + request.getCnp().charAt(1) + request.getCnp().charAt(2));
        }
        else if(request.getCnp().charAt(0)=='6') {
            System.out.println(request.getName());
            System.out.println("Femeie nascuta in " + request.getCnp().charAt(5) + request.getCnp().charAt(6) + "/" + request.getCnp().charAt(3) + request.getCnp().charAt(4) + "/20" + request.getCnp().charAt(1) + request.getCnp().charAt(2));
        }
        System.out.println("\n");
        PersoanaOuterClass.NumeSiCNPReply reply = PersoanaOuterClass.NumeSiCNPReply.newBuilder().setMessage("Operatie efectuata cu succes.").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }


}
//5000510297270
//0123456789